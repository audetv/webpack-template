const webpack = require('webpack')
const { merge } = require('webpack-merge');
const baseWebpackConfig = require('./webpack.common')

const devWebpackConfig = merge(baseWebpackConfig, {
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    host: '0.0.0.0',
    port: '3000',
    contentBase: baseWebpackConfig.externals.paths.dist,
    watchContentBase: true,
    hot: true,
    overlay: {
      warnings: true,
      errors: true
    }
  },
  plugins: [],
})

module.exports = new Promise((resolve, reject) => {
  resolve(devWebpackConfig)
})
