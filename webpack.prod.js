const { merge } = require('webpack-merge');
const baseWebpackConfig = require('./webpack.common')
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const TerserPlugin = require("terser-webpack-plugin");

const buildWebpackConfig = merge(baseWebpackConfig, {
  mode: 'production',
  optimization: {
    minimize: true,
    minimizer: [
      new CssMinimizerPlugin(),
      new TerserPlugin()
    ],
    splitChunks: {
      cacheGroups: {
        vendor: {
          name: 'vendors',
          test: /[\\/]node_modules[\\/]/,
          priority: -10,
          reuseExistingChunk: true,
          chunks: 'all',
          enforce: true
        }
      }
    },
  },
  plugins: [],
})

module.exports = new Promise((resolve, reject) => {
  resolve(buildWebpackConfig)
})
