const form = document.querySelector('form')

form.addEventListener('submit', (e) => {
  e.preventDefault()
  console.log(e.target)
  const formData = new FormData(e.target)
  console.log(formData)
  const json = JSON.stringify(Object.fromEntries(formData));
  console.log(json)
})
