# Webpack Template

Для запуска на wsl2.
Получаем ip адрес wsl2 виртуальной машины.
В консоли wsl2 набираем:

`ip addr | grep eth0` 

Получим inet ip адрес:

`4: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
inet 172.26.98.36/20 brd 172.26.111.255 scope global eth0`

В webpack.config.js в секции devServer
```
devServer: {
    host: '0.0.0.0', // обязательно
    port: '3000', // любой порт
    contentBase: './dist',
},
```
В браузере сборка будет доступна по адресу:

`http://172.26.98.36:3000`
